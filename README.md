"# This is my README" 

This is my final year project, provisionally named CES (Cardiac Evaluation System), which provides access control features
based on a CBAC (Category Based Access Control) model. The program simulates a normal heart rate for 45 seconds, which then
elevates for 25 seconds to simulate an "emergency" situation.
