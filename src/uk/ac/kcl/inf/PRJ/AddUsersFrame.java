package uk.ac.kcl.inf.PRJ;

import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.DefaultComboBoxModel;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Provides the interface to add a user into the system.
 * @author Dimeji
 *
 */
@SuppressWarnings("serial")
public class AddUsersFrame extends JFrame {

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField;
	private JTextField textField_3;
	private JTextField textField_4;
	private String[] addUserInfo;

	/**
	 * Create the application.
	 */
	public AddUsersFrame() {
		super("Add User(s)");
		
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		setBounds(100, 100, 500, 300);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addUserInfo = new String[7];
		getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("31px"),
				ColumnSpec.decode("95px"),
				FormFactory.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("55px"),
				FormFactory.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("231px"),},
			new RowSpec[] {
				RowSpec.decode("31px"),
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				RowSpec.decode("21px"),
				RowSpec.decode("23px"),}));
		
		JLabel lblEmployeeId = new JLabel("Employee ID:");
		lblEmployeeId.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(lblEmployeeId, "2, 2, fill, center");
		
		textField_2 = new JTextField();
		getContentPane().add(textField_2, "4, 2, 3, 2, fill, top");
		textField_2.setColumns(10);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(lblFirstName, "2, 4, fill, center");
		
		textField = new JTextField();
		getContentPane().add(textField, "4, 4, 3, 2, fill, top");
		textField.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(lblLastName, "2, 6, fill, center");
		
		textField_1 = new JTextField();
		getContentPane().add(textField_1, "4, 6, 3, 2, fill, top");
		textField_1.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblPassword, "2, 8, right, center");
		
		passwordField = new JPasswordField();
		getContentPane().add(passwordField, "4, 8, 3, 2, fill, top");
		
		JLabel lblJob = new JLabel("Job:");
		lblJob.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblJob, "2, 10, right, center");
		
		textField_3 = new JTextField();
		getContentPane().add(textField_3, "4, 10, 3, 2, fill, top");
		textField_3.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblCategory, "2, 12, right, center");
		
		final JComboBox comboBox = new JComboBox();
		try {
			comboBox.setModel(new DefaultComboBoxModel(CategoryFrame.getCategoriesList()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		getContentPane().add(comboBox, "4, 12, 3, 2, fill, top");
		
		JLabel lblPhoneNumber = new JLabel("Phone Number:");
		lblPhoneNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(lblPhoneNumber, "2, 14, fill, center");
		
		textField_4 = new JTextField();
		getContentPane().add(textField_4, "4, 14, 3, 2, fill, top");
		textField_4.setColumns(10);
		
		JButton btnAddUser = new JButton("Add User");
		btnAddUser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DBConnection dbi = new DBConnection();
				try{
					Integer.parseInt(textField_2.getText());
					addUserInfo[0] = textField_2.getText(); //EID
					addUserInfo[1] = textField.getText(); //First Name
					addUserInfo[2] = textField_1.getText();//Last Name

					String pWord = "";
					for(int i = 0; i < passwordField.getPassword().length; i++){
						pWord = pWord + passwordField.getPassword()[i];
					}
					addUserInfo[3] = DigestUtils.md5Hex(pWord); //Password
					addUserInfo[4] = textField_3.getText(); //Job
					addUserInfo[5] = comboBox.getSelectedItem().toString(); //Category
					addUserInfo[6] = textField_4.getText(); //Phone Number

					dbi.insertUser(addUserInfo);
					JOptionPane.showMessageDialog(new JFrame(), "User has been successfully added!");
                    dispose();

				}catch(NumberFormatException error){
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid EID!");
				}
				
				
			}
		});
		getContentPane().add(btnAddUser, "2, 16, 3, 1, right, top");
		
		JButton btnReturnToCes = new JButton("Return to CES");
		btnReturnToCes.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		getContentPane().add(btnReturnToCes, "6, 16, left, top");
		
		setVisible(true);
	}
}
