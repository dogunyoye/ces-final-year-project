package uk.ac.kcl.inf.PRJ;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * Initialises the options interface for CES. Containing functions such as 
 * Add Patient, Add Category, Set Emergency, 
 * @author Dimeji
 *
 */
@SuppressWarnings("serial")
public class OptionFrame extends JFrame {

	/**
	 * Create the application.
	 */
	public OptionFrame() {
        super(LoginSystem.employee.toString());
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(100, 100, 450, 220);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
			
		String userDir = System.getProperty("user.home").replaceAll("\\\\", "/");

		File f = new File(userDir + "/CESFiles");
		File logDir = new File(userDir + "/CESFiles/Patient Emergency Logs");
		f.mkdir();
		logDir.mkdir();
		File policy = new File(userDir + "/CESFiles/policy.txt");
		File emg_policy = new File(userDir + "/CESFiles/emg_policy.txt");
        
		if(!policy.exists() && !emg_policy.exists()){
			try {
				policy.createNewFile();
				emg_policy.createNewFile();
				FileWriter f_1 = new FileWriter(policy,true);
				FileWriter f_2 = new FileWriter(emg_policy,true);
				BufferedWriter policyWriter = new BufferedWriter(f_1);
				BufferedWriter emgWriter = new BufferedWriter(f_2);
				policyWriter.write("Green - Patient Status,Employee List,Medical Record,Emergency Log File");
				policyWriter.newLine();
				policyWriter.write("Blue - Employee List,");
				emgWriter.write("Blue,Green");
				policyWriter.close();
				emgWriter.close();
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		JButton btnCes = new JButton("CES");
		btnCes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(MainSystem.isPatientSet){
					new MainSystem();
					dispose();
				}
				else{
					JOptionPane.showMessageDialog(new JFrame(), "You must add a patient!");
				}
			}
		});
		getContentPane().add(btnCes, "6, 4");

		JLabel lblViewCes = new JLabel("View CES");
		getContentPane().add(lblViewCes, "10, 4");
		
				JButton btnCategories = new JButton("Categories");
				btnCategories.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						new CategoryFrame();
					}
				});
				getContentPane().add(btnCategories, "6, 8");		
				JLabel lblCreateUserCategories = new JLabel("Create user categories");
				getContentPane().add(lblCreateUserCategories, "10, 8");
				JButton btnEmergency = new JButton("Emergency");
				btnEmergency.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						new EmergencyFrame();
					}
				});
				getContentPane().add(btnEmergency, "6, 12");
				JLabel lblSetPatientEmergency = new JLabel("Set patient emergency boundaries");
				getContentPane().add(lblSetPatientEmergency, "10, 12");
		
				JButton btnPatient = new JButton("Patient");
				btnPatient.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						new PatientFrame();
					}
				});
				getContentPane().add(btnPatient, "6, 16");
		
				JLabel lblAddAParient = new JLabel("Add a parient to be monitored");
				getContentPane().add(lblAddAParient, "10, 16");

		setVisible(true);
	}

}
