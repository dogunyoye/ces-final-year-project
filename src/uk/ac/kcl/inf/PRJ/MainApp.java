package uk.ac.kcl.inf.PRJ;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Class to initiate CES login system.
 * @author Dimeji
 *
 */
public class MainApp {
	
	public MainApp(){
		InputStream stream = MainApp.class.getResourceAsStream("/settings/cesdatabase.db");
		InputStream stream_1 = MainApp.class.getResourceAsStream("/settings/emg_sound.wav");
		InputStream stream_2 = MainApp.class.getResourceAsStream("/settings/medfile.txt");
		String workingDir = System.getProperty("user.dir").replaceAll("\\\\", "/");
		File settingsDir = new File(workingDir + "/settings");
		settingsDir.mkdir();
	    if (stream == null) {
	    	
	    }
	    OutputStream resStreamOut = null, resStreamOut_1 = null, resStreamOut_2 = null;
	    int readBytes;
	    byte[] buffer = new byte[4096];
	    try {
	    	File f = new File(workingDir + "/settings/cesdatabase.db");
	    	File f_1 = new File(workingDir + "/settings/emg_sound.wav");
	    	File f_2 = new File(workingDir + "/settings/medfile.txt");

	    	if(!f.exists()){
	    		resStreamOut = new FileOutputStream(f);
	    		while ((readBytes = stream.read(buffer)) > 0) {
	    			resStreamOut.write(buffer, 0, readBytes);
	    		}
	    	}

	    	if(!f_1.exists()){
	    		resStreamOut_1 = new FileOutputStream(f_1);
	    		while ((readBytes = stream_1.read(buffer)) > 0) {
	    			resStreamOut_1.write(buffer, 0, readBytes);
	    		}

	    	}

	    	if(!f_2.exists()){
	    		resStreamOut_2 = new FileOutputStream(f_2);
	    		while ((readBytes = stream_2.read(buffer)) > 0) {
	    			resStreamOut_2.write(buffer, 0, readBytes);
	    		}

	    	}
	    	
	    } catch (IOException e1) {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
	        JOptionPane.showMessageDialog(new JFrame(), e1);
	    } finally {
	        try {
				stream.close(); 
				if(!(resStreamOut == null)){
				resStreamOut.close();
				    }
				if(!(resStreamOut_1 == null)){
					resStreamOut_1.close();
					}
				if(!(resStreamOut_2 == null)){
					resStreamOut_2.close();
					}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JOptionPane.showMessageDialog(new JFrame(), e);
			}
	       
	    }
	}

	/**
	 * Main method to initiate the system
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MainApp();
		new LoginSystem();
	}

}
