package uk.ac.kcl.inf.PRJ;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.SwingConstants;
/**
 * Provides an interface to add a patient into the monitoring system.
 * Must be done before the monitoring system is started.
 * @author Dimeji
 *
 */
@SuppressWarnings("serial")
public class PatientFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	
	public PatientFrame(){
		super("Add patient");
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		initialize();
	}

	
	/**
	 * Create the frame.
	 */
	private void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 510, 260);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		
		JLabel lblPatientName = new JLabel("Patient Name:");
		contentPane.add(lblPatientName, "4, 4, right, default");
		
		textField = new JTextField();
		contentPane.add(textField, "6, 4, left, default");
		textField.setColumns(10);
		
		JLabel lblPatientLastName = new JLabel("Patient Last Name:");
		contentPane.add(lblPatientLastName, "4, 6, right, default");
		
		textField_2 = new JTextField();
		contentPane.add(textField_2, "6, 6, fill, default");
		textField_2.setColumns(10);
		
		JLabel lblPatientId = new JLabel("Patient ID:");
		lblPatientId.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblPatientId, "4, 8, right, default");
		
		textField_3 = new JTextField();
		contentPane.add(textField_3, "6, 8, left, default");
		textField_3.setColumns(10);
		
		JLabel lblMedicalFile = new JLabel("Medical File:");
		contentPane.add(lblMedicalFile, "4, 10, right, default");
		
		textField_1 = new JTextField();
		contentPane.add(textField_1, "6, 10, fill, default");
		textField_1.setColumns(10);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser f = new JFileChooser();
				f.showOpenDialog(PatientFrame.this);
				File file = f.getSelectedFile();
				try{
				textField_1.setText(file.getAbsolutePath());
				}
				catch(NullPointerException ex){
					System.out.println("Caught Null");
				}
			}
		});
		contentPane.add(btnBrowse, "8, 10");
		
		JPanel panel = new JPanel();
		contentPane.add(panel, "6, 14, left, fill");
		
		JButton btnNewButton = new JButton("Add Patient");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!textField_1.getText().isEmpty()){
				String name = textField.getText() + textField_2.getText();

				for (int i = 0; i < name.length(); i++) {
				    char c = name.charAt(i);
				    if(Character.isUpperCase(c)){
				    	MainSystem.emergencylogName += c;
				    }
				}
				
				MainSystem.emergencylogName += textField_3.getText() + ".txt";
				MainSystem.patientName = textField.getText() + " " + textField_2.getText();
				MainSystem.medfileName = textField_1.getText().replace('\\', '/');
				MainSystem.isPatientSet = true;
				JOptionPane.showMessageDialog(new JFrame(), "Patient Successfully added!");
				dispose();
				}
				else{
					JOptionPane.showMessageDialog(new JFrame(), "You must provide a valid patient medical file!");
				}
			}
		});
		panel.add(btnNewButton);		
		JButton btnNewButton_1 = new JButton("Return to CES");
		btnNewButton_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel.add(btnNewButton_1);
		setVisible(true);
	}
}
