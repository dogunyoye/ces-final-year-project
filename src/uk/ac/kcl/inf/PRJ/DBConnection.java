package uk.ac.kcl.inf.PRJ;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Sets up the database connection of the system and also provides
 * methods for database functionality.
 */
public class DBConnection {
	
	private Connection con;
    private PreparedStatement pst;
    private ResultSet rs;
    private String url;
    private String workingDir = System.getProperty("user.dir").replaceAll("\\\\", "/");
    
    protected String[][] employees_table;
    
    public DBConnection(){
    	
    	String sqliteDriver = "org.sqlite.JDBC";
        try {
			Class.forName(sqliteDriver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	con = null;
        pst = null;
        rs = null;
       // String workingDir = System.getProperty("user.dir").replaceAll("\\\\", "/");    
        
        url = "jdbc:sqlite:" + workingDir + "/settings/cesdatabase.db";
        
        employees_table = new String[getRows()][5];
    }
    /**
     * Returns the number of rows in database. Each row represents
     * an individual employee
     * @return Number of rows (employees) in the database table
     */
    public int getRows(){

    	int rows = 0;
    	try {

    		con = DriverManager.getConnection(url);
    		pst = con.prepareStatement("SELECT COUNT(*) FROM employees");
    		rs = pst.executeQuery();

    		while (rs.next()) {
    			rows = rs.getInt(1);
    		}

    	} catch (SQLException ex) {
    		Logger lgr = Logger.getLogger(DBConnection.class.getName());
    		lgr.log(Level.SEVERE, ex.getMessage(), ex);

    	} 
    	return rows;
    }
    
    /**
     * Sets up the system's employee JTable
     */
    public void setEmployeesTable(){
    	try {

    		con = DriverManager.getConnection(url);
    		pst = con.prepareStatement("SELECT * FROM employees");
    		rs = pst.executeQuery();
    		int i = 0;
    		while (rs.next()) {
    			employees_table[i][0] = rs.getString(2); //First Name
    			employees_table[i][1] = rs.getString(3); //Last Name
    			employees_table[i][2] = rs.getString(5); //Job
    			employees_table[i][3] = rs.getString(6); //Category
    			employees_table[i][4] = rs.getString(7); //Phone Number
    			i++;
    		}

    	} catch (SQLException ex) {
    		Logger lgr = Logger.getLogger(DBConnection.class.getName());
    		lgr.log(Level.SEVERE, ex.getMessage(), ex);
    	} 

    }
    
    /**
     * Returns the user's password (stored as an MD5 hash in the database table). 
     * Used primarily for authenticating users.
     * @param username - username (EID) of the employee
     * @return The user's password (MD5 hash)
     */
    public String getPassword(String username){
    	
    	String ePassword = "";
    	
    	try {

    		con = DriverManager.getConnection(url);
    		pst = con.prepareStatement("SELECT Password FROM employees WHERE EID = '" + username + "'");
    		rs = pst.executeQuery();
    		   		
    		while (rs.next()) {
    			ePassword = rs.getString(1);
    		}
    			

    	} catch (SQLException ex) {
    		Logger lgr = Logger.getLogger(DBConnection.class.getName());
    		lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		JOptionPane.showMessageDialog(new JFrame(), ex);
    	} 
    	
    	return ePassword;

    }
    
    /**
     * Returns an array containing information on a logged in user
     * @param username - username (EID) of the employee
     * @return An array containing the EID, First Name, Last Name, Job,
     * Category and Phone Number of an employee.
     */
    public String[] currentEmployee(String username){
    	
    	String[] employeeInfo = new String[6];
    	
    	try {

    		con = DriverManager.getConnection(url);
    		pst = con.prepareStatement("SELECT * FROM employees WHERE EID ='" + username + "'");
    		rs = pst.executeQuery();
    		
    		while (rs.next()) {
    			employeeInfo[0] = rs.getString(1); //EID
    			employeeInfo[1] = rs.getString(2); //First Name
    			employeeInfo[2] = rs.getString(3); //Last Name
    			employeeInfo[3] = rs.getString(5); //Job
    			employeeInfo[4] = rs.getString(6); //Category
    			employeeInfo[5] = rs.getString(7); //Phone Number
    			
    		}

    	} catch (SQLException ex) {
    		Logger lgr = Logger.getLogger(DBConnection.class.getName());
    		lgr.log(Level.SEVERE, ex.getMessage(), ex);
    	}
    	
    	return employeeInfo;
    	
    }
    
    /**
     * Adds an user into the system.
     * @param userInfo - an array storing information on the user to be 
     * added (taken from the Add Users form)
     */
    public void insertUser(String[] userInfo){	

    	try {

    		con = DriverManager.getConnection(url);
    		pst = con.prepareStatement("INSERT INTO employees(EID, FName, LName, Password, Job, Category, PNumber) VALUES(?,?,?,?,?,?,?)");
    		pst.setInt(1, Integer.parseInt(userInfo[0]));
    		for(int i = 1; i < userInfo.length; i++){
    			pst.setString(i+1, userInfo[i]);
    		}
    		pst.executeUpdate();		

    	} catch (SQLException ex) {
    		Logger lgr = Logger.getLogger(DBConnection.class.getName());
    		lgr.log(Level.SEVERE, ex.getMessage(), ex);
    	}
    	
    }
    
    /**
     * Updates a User's category
     * @param cat - Name of the category to be changed into
     * @param eID - EID of the user
     */
    public void updateUserCategory(String cat, String eID){
    	
    	try {
    		con = DriverManager.getConnection(url);
    		pst = con.prepareStatement("UPDATE employees SET Category = '" + cat + "' WHERE EID = " + eID);
    		pst.executeUpdate();		

    	} catch (SQLException ex) {
    		Logger lgr = Logger.getLogger(DBConnection.class.getName());
    		lgr.log(Level.SEVERE, ex.getMessage(), ex);
    	}
    	
    }
    
    

}
