package uk.ac.kcl.inf.PRJ;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

/**
 * Creates an interface that allows an authenticated user to create a category with permissions
 * and add that category to the local policy.txt file.
 * @author Dimeji
 *
 */

@SuppressWarnings("serial")
public class CategoryFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private static String policyDir = System.getProperty("user.home").replaceAll("\\\\", "/") + "/CESFiles/policy.txt";
	
	/**
	 * Create the application.
	 */
	public CategoryFrame(){
		super("Add a category");
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		initialize();		
	}

	/**
	 * Initialize the frame's content..
	 */
	private void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(77, 16, 149, 174);
		contentPane.add(panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		textField = new JTextField();
		panel.add(textField, "2, 2, fill, default");
		textField.setColumns(10);
		
		final JCheckBox chckbxNewCheckBox = new JCheckBox("Patient Status");
		panel.add(chckbxNewCheckBox, "2, 4");
		
		final JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Employee List");
		panel.add(chckbxNewCheckBox_1, "2, 6");
		
		final JCheckBox chckbxNewCheckBox_2 = new JCheckBox("Medical Record");
		panel.add(chckbxNewCheckBox_2, "2, 8");
		
		final JLabel lblInfo = new JLabel();
		lblInfo.setBounds(10, 188, 229, 81);
		contentPane.add(lblInfo);
		
		final JCheckBox chckbxEmergencyLogFile = new JCheckBox("Emergency Log File");
		panel.add(chckbxEmergencyLogFile, "2, 10");
		
		JButton btnAddCategory = new JButton("Add Category");
		panel.add(btnAddCategory, "2, 12");
		btnAddCategory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				File f = new File(policyDir);

				try{

				    FileWriter fileWriter = new FileWriter(f, true);

				    BufferedWriter Writer = new BufferedWriter(fileWriter);
				    String data = textField.getText() + " - ";
				    if(chckbxNewCheckBox.isSelected()){
				    	data = data + chckbxNewCheckBox.getText() + ",";
				    }
				    if(chckbxNewCheckBox_1.isSelected()){
				    	data = data + chckbxNewCheckBox_1.getText() + ",";
				    }
				    if(chckbxNewCheckBox_2.isSelected()){
				    	data = data + chckbxNewCheckBox_2.getText() + ",";
				    }
				    if(chckbxEmergencyLogFile.isSelected()){
				    	data = data + chckbxEmergencyLogFile.getText();
				    }
				    Writer.write(data);
				    Writer.newLine();
				    Writer.close();
				    
				} catch(IOException err) {
				    System.out.println(err);
				}
			} 
		});
		
		final JList list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				
				BufferedReader in = null;
				String str=null;
				try {
					in = new BufferedReader(new FileReader(policyDir));
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			    
			   
			    try {
					while((str = in.readLine()) != null){
						if(str.contains(list.getSelectedValue().toString())){
							String[] access = str.split("-");
							String[] actions = access[1].split(",");
							String lblText = "<html>";
							for(int i = 0; i < actions.length; i++){
								lblText = lblText + actions[i] + "<br>";
							}
							lblText = lblText + "</html>";
							lblInfo.setText(lblText);
						}				  
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			  
				
			}
		});
		try {
			list.setModel(new AbstractListModel() {
				
				String[] values = getCategoriesList();
				public int getSize() {
					return values.length;
				}
				public Object getElementAt(int index) {
					return values[index];
				}
			});
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		JScrollPane listPane = new JScrollPane(list);
		listPane.setBounds(249, 16, 175, 253);
		contentPane.add(listPane);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(39, 29, 60, 14);
		contentPane.add(lblName);
				
		setVisible(true);
	}
	
	/**
	 * Retrieves category lines in the policy file and places each line in an array
	 * @return An array containing categories and their corresponding permissions
	 * @throws IOException
	 */
	public static String[] getCategoriesList() throws IOException{
		BufferedReader in = new BufferedReader(new FileReader(policyDir));
	    String str=null;
	    ArrayList<String> lines = new ArrayList<String>();
	    while((str = in.readLine()) != null){
	    	String[] catName = str.split(" ");
	        lines.add(catName[0]);
	    }
	   String[] linesArray = lines.toArray(new String[lines.size()]);
	   return linesArray;
	}
}
