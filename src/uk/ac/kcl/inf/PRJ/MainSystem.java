package uk.ac.kcl.inf.PRJ;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 * Launches the main CES, containing heart rate monitoring patient status,
 * patient medical history file, employees table and patient emergency log
 * @author Dimeji
 *
 */
@SuppressWarnings("serial")
public class MainSystem extends JFrame {

	private JLabel textLabel;
	private JTextArea medArea;
	private JTextArea logArea;
	private JTable employees;
	private JTabbedPane tabbedPane;
	private String[] ColumnNames;
	private String[] accessRights = null;
	private int threadLife = 0;
    private DBConnection testcon; 
    private SimpleDateFormat date;
    private String access = "";
    private String userDirectory = System.getProperty("user.home").replaceAll("\\\\", "/") + "/CESFiles/";
    private String workingDirectory = System.getProperty("user.dir").replaceAll("\\\\", "/");
    
    private volatile String hour_val;
    private volatile String minute_val;
    private volatile String second_val;
    
    private Random rand; 
    private volatile int heartRate;
    private volatile boolean isEmergency;
    protected static int maxHR = 180;
    protected static String patientName = "";
    protected static String medfileName = "";
    protected static String emergencylogName = "";
    protected static boolean isPatientSet = false;

	/**
	 * Create the application.
	 */
	public MainSystem() {
		
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		testcon = new DBConnection();
		testcon.setEmployeesTable();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(250, 250, 600, 450);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		rand = new Random();
		isEmergency = false;
		setTitle("CES " + LoginSystem.employee.toString()); //ensures that a login is required before the system is accessed
		ColumnNames = new String[]{"First Name","Last Name","Job","Category","Phone Number"};
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		
		tabbedPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		employees = new JTable(testcon.employees_table, ColumnNames);
		tabbedPane.addTab("Patient Status", panel1);
		tabbedPane.addTab("Employees", panel2);
		tabbedPane.addTab("Medical History", panel3);
		tabbedPane.addTab("Patient Emergency Log", panel4);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
		
		textLabel = new JLabel();
		textLabel.setHorizontalAlignment(SwingConstants.CENTER);
		medArea = new JTextArea();
		logArea = new JTextArea();
		medArea.setEditable(false);
        logArea.setEditable(false);
		medArea.setRows(20);
		logArea.setRows(20);
		
		JScrollPane textPane = new JScrollPane(textLabel);
		JScrollPane medPane = new JScrollPane(medArea);
		JScrollPane empPane = new JScrollPane(employees);
		JScrollPane logPane = new JScrollPane(logArea);
		textPane.setPreferredSize(new Dimension(200, 150));
		medPane.setPreferredSize(new Dimension(550, 320));
		empPane.setPreferredSize(new Dimension(550,350));
		logPane.setPreferredSize(new Dimension(550,320));
		
		Font font = new Font("Verdana", Font.BOLD, 25);
		textLabel.setFont(font);
		
		
		setPatientFiles();
		medArea.setCaretPosition(0);
		
		panel1.add(textPane);
		panel2.add(empPane);
		panel3.add(medPane);
		panel4.add(logPane);
		
		getContentPane().add(tabbedPane, BorderLayout.NORTH);
		
		try {
			evaluateAccess();
			tabbedPane.setSelectedIndex(Integer.parseInt("" + access.charAt(0)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
		if(accessRights.length != 4){ //Only users without full access to the system need to be updated 
		   emergencyThread();	
		 }

		patientThread(patientName);
		emergencyAlert();
		setVisible(true);

	}
	
	/**
	 * Simulates patient monitoring
	 * @param name
	 */
	private void patientThread(final String name){
		Thread patientThread = new Thread(){
			public void run(){
				while(true){
				Calendar cal = Calendar.getInstance();
		int hournum = Integer.parseInt(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
		int minutenum = Integer.parseInt(String.valueOf(cal.get(Calendar.MINUTE)));
		int secondnum = Integer.parseInt(String.valueOf(cal.get(Calendar.SECOND)));
		hour_val = String.format("%02d", hournum);
		minute_val = String.format("%02d", minutenum);
		second_val = String.format("%02d", secondnum);
		
		if(threadLife >= 45 && threadLife <= 70){ //opens access for 25seconds

			heartRate = rand.nextInt(20) + maxHR; // heartrate simulation - emergency
			isEmergency = true;
			textLabel.setForeground(Color.RED);
				}
				else {
				heartRate = rand.nextInt(80) + 20; //heartrate simulation - normal 80-160
				isEmergency = false;
				textLabel.setForeground(Color.GREEN);
				}
				
				textLabel.setText("<html><center>" + name + ", HR: " + heartRate + "<br><font size=\"4\">Last Updated: " + hour_val + ":" + minute_val + ":" + second_val +"</font><br></center></html>");
					
				try{
					Thread.sleep(5000);
					threadLife = threadLife + 5;
				}
				catch(InterruptedException e){
					System.out.print(e);
				}
			  }
			}
		};
		patientThread.start();
	}
	
	/**
	 * Re-evaluates users, who don't have full access to the system, during emergencies. 
	 */
	private void emergencyThread(){
		Thread emgThread = new Thread(){
	public void run(){
		while(true){
			if(isEmergency){			
				while(isEmergency); 
		testcon.
updateUserCategory(LoginSystem.employee.getCategory(),LoginSystem.employee.getEID()); 
				access = "";
				try {
					evaluateAccess();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(!access.contains("" + tabbedPane.getSelectedIndex())){
					tabbedPane.setSelectedIndex(Integer.parseInt("" + access.charAt(0))); 
				}
				
				
									
			}
			else{	
				
				while(!isEmergency); 
				access = "";
				try {
		testcon.
updateUserCategory(getEmergencyAccess(),LoginSystem.employee.getEID()); 
					evaluateAccess();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} 
			
				DefaultTableModel updatedTable = new DefaultTableModel();
				testcon.setEmployeesTable();
				try{
					Thread.sleep(3000);
				}catch(InterruptedException e){
					
				}
				updatedTable.setDataVector(testcon.employees_table, ColumnNames);
				employees.setModel(updatedTable);
						    
		}
	}

		};
		emgThread.start();
	}
	
	/**
	 * Initiates a message dialog box alerting the user of an emergency.
	 */
	private void emergencyAlert(){
		Thread emgAlert = new Thread(){
			public void run(){
				while(true){
					if(isEmergency){					
					    logEmergency(); //emergency logged
					    AudioStream as = null;
					    try {
							as = 
								new AudioStream(new FileInputStream(workingDirectory + "/settings/emg_sound.wav"));
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					    AudioPlayer.player.start(as);
					    JOptionPane.showMessageDialog(new JFrame(), patientName + " is undergoing an emergency!");						    
						while(isEmergency); //spins here while in emergency
						JOptionPane.showMessageDialog(new JFrame(), patientName + " has recovered");
					}
				}
			}
		};
		emgAlert.start();
	}
	
	/**
	 * Sets the correct interface for the logged in user by retrieving his/her category and
	 * evaluating it against the policy file. Thus tabs will be open or blocked based on 
	 * policy evaluation.
	 * @throws IOException If the policy file is unavailable, this exception is thrown
	 */
	private void evaluateAccess() throws IOException{
		
		tabbedPane.setEnabledAt(0, false);
		tabbedPane.setEnabledAt(1, false);
		tabbedPane.setEnabledAt(2, false);
		tabbedPane.setEnabledAt(3, false);
		access = "";
		
		BufferedReader in = new BufferedReader(new FileReader(userDirectory + "policy.txt"));
	    String str=null;
	    String[] category = null;
	    
	    while((str = in.readLine()) != null){
	    	category = str.split("-");
	    if(category[0].contains(testcon.currentEmployee(LoginSystem.employee.getEID())[4])){
	    		
	    		accessRights = category[1].split(",");
	    		for(int i = 0; i < accessRights.length; i++){
	    		if(accessRights[i].contains("Patient Status")){
	    			tabbedPane.setEnabledAt(0, true);
	    			access += "0";
	    		 }
	    		if(accessRights[i].contains("Employee List")){
	    			tabbedPane.setEnabledAt(1, true);
	    			access += "1";
	    		 }
	    		if(accessRights[i].contains("Medical Record")){
	    			tabbedPane.setEnabledAt(2, true);
	    			access += "2";
	    		 }
	    		if(accessRights[i].contains("Emergency Log File")){
	    			tabbedPane.setEnabledAt(3, true);
	    			access += "3";
	    		 }
	    		}
	    		
	    	}
	    }
	    
	}
	
	/**
	 * Initiates the set patient files to be viewable in the system's tabs.
	 */
	private void setPatientFiles(){
		
		File medFile = new File(medfileName);
		File logFile = new File(userDirectory + "Patient Emergency Logs/" + emergencylogName);
		FileInputStream stream = null;
		FileInputStream stream_1 = null;

		try {
			stream = new FileInputStream(medFile);
            
			int content;
			while ((content = stream.read()) != -1) {
				// med file stream
				medArea.append("" + (char) content);
			}
			if(logFile.exists()){
				
				stream_1 = new FileInputStream(logFile);
				while ((content = stream_1.read()) != -1) {
					// log file stream
					logArea.append("" + (char) content);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stream != null){
				stream.close();
				}
				if(stream_1 != null){
					stream_1.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Logs each time an emergency occurs. Log file is named with the patient's initials
	 * concatenated with his/her patient ID.
	 */
	private void logEmergency(){
		
		File f = new File(userDirectory + "Patient Emergency Logs/" + emergencylogName);
		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = date.format(new Date());
		
		try{
			
		    FileWriter fw = new FileWriter(f, true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    String data = dateString + " " + LoginSystem.employee.getEID() + " " + heartRate;		    
		    bw.write(data);
		    bw.newLine();
		    bw.close();
		    
		} catch(IOException err) {
		    System.out.println(err);
		}
		
	}
	
	private String getEmergencyAccess() throws IOException{
		
		BufferedReader in = new BufferedReader(new FileReader(userDirectory + "emg_policy.txt"));
	    String str=null;
	    String destCategory = null;
	    
	    while((str = in.readLine()) != null){
	    	if(str.startsWith(LoginSystem.employee.getCategory())){
	    	destCategory = str.split(",")[1];
	    	}
	    }
	    
	    return destCategory;
	    	
	}
	
	
	  protected JComponent makeTextPanel(String text) {
	        JPanel panel = new JPanel(false);
	        JLabel filler = new JLabel(text);
	        filler.setHorizontalAlignment(JLabel.CENTER);
	        panel.setLayout(new GridLayout(1, 1));
	        panel.add(filler);
	        return panel;
	    }
}
