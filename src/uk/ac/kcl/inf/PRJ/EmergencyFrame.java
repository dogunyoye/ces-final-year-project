package uk.ac.kcl.inf.PRJ;



import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Provides an interface for a user to adjust the heart rate value
 * an emergency is initiated at.
 * @author Dimeji
 *
 */
@SuppressWarnings("serial")
public class EmergencyFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	/**
	 * Creates the application
	 */

	public EmergencyFrame(){
		super("Set emergency heart rate");
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		initialize();
	}

	/**
	 * Initializes the frame's content.
	 */
	private void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 450, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("117px"),
				ColumnSpec.decode("109px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("86px"),},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblEmergencyHeartRate = new JLabel("Emergency Heart Rate:");
		lblEmergencyHeartRate.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(lblEmergencyHeartRate, "1, 2, 2, 2, right, center");
		
		textField = new JTextField();
		contentPane.add(textField, "4, 2, 1, 2, right, top");
		textField.setColumns(10);
		
		JButton btnSetHeartRate = new JButton("Set Heart Rate");
		btnSetHeartRate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				MainSystem.maxHR = Integer.parseInt(textField.getText());
				}
				catch(NumberFormatException err){
					JOptionPane.showMessageDialog(new JFrame(), err);
				}
			}
		});
		contentPane.add(btnSetHeartRate, "2, 6");
		
		setVisible(true);
	}

}
