package uk.ac.kcl.inf.PRJ;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.apache.commons.codec.digest.DigestUtils;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JPanel;

/**
 * Creates a Login interface based on credentials stores in the local cesdatabase.db file.
 * The interface takes a username (EID) and password. The password field of the local database
 * stores an MD5 hash, thus if passwords are forgotten they are irretrievable. The user guide 
 * contains the plaintext passwords of the 4 default accounts.
 * @author Dimeji
 *
 */

@SuppressWarnings("serial")
public class LoginSystem extends JFrame {

	private JTextField textField;
	private JPasswordField passwordField;
	private JButton btnLogin;
	protected static Employee employee;
	private JPanel panel;
	private JButton btnNewButton;

	/**
	 * Create the application.
	 */
	public LoginSystem() {
		super("Welcome to CES");
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		SwingUtilities.updateComponentTreeUI(this);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(100, 100, 320, 170);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		
		JLabel lblUsername = new JLabel("User Name:");
		getContentPane().add(lblUsername, "2, 2, right, default");
		
		textField = new JTextField();
		getContentPane().add(textField, "6, 2, fill, default");
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Password:");
		getContentPane().add(lblNewLabel, "2, 4, right, default");
		
		passwordField = new JPasswordField();
		
		getContentPane().add(passwordField, "6, 4, fill, default");
		
		panel = new JPanel();
		getContentPane().add(panel, "6, 8, fill, fill");
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		btnLogin = new JButton("Login");
		panel.add(btnLogin, "2, 2");
		
		btnLogin.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String username = textField.getText();
				String password = "";
				char[] passwordArr = passwordField.getPassword();
				
				DBConnection LoginDB = new DBConnection();
				for(int i = 0; i < passwordArr.length; i++){
					password = password + passwordArr[i];
				}
				
				String digest = DigestUtils.md5Hex(password);
				
				if(LoginDB.getPassword(username).equals(digest)){
					String[] eInfo = LoginDB.currentEmployee(username);
					employee = new Employee(eInfo[0], eInfo[1], eInfo[2], eInfo[3], eInfo[4], eInfo[5]);
					new OptionFrame();
					dispose();
				}
				else{
					JOptionPane.showMessageDialog(new JFrame(), "Your username/password is invalid");
				}
				
			}
			
		});
		
		btnNewButton = new JButton("Add User");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AddUsersFrame();
			}
		});
		panel.add(btnNewButton, "8, 2");
		setVisible(true);
	}

}
