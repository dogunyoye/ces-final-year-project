package uk.ac.kcl.inf.PRJ;

import static org.junit.Assert.*;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

/**
 * Unit testing class for the system's database connection.
 * @author Dimeji
 *
 */
public class DBConnectionTest {
	
private DBConnection DBTest = new DBConnection();
private String username = "1114460";

    /**
     * Test for a valid username, returns the corresponding MD5 hash with matches.
     */
	@Test
	public void testDBPassword(){
		String passwordDigest = DigestUtils.md5Hex("jpeters1"); //known password of Jeremy Peters, retrieving MD5 hash
		 //known EID of Jeremy Peters
		assertEquals(passwordDigest, DBTest.getPassword(username));
	}
	
	/**
	 * Test for an invalid username. The username is a string of characters as opposed to numbers
	 * which will return an empty string (no result from the database).
	 *
	 */
	@Test 
	public void testDBPasswordIncorrectUsername(){  
		String invalidUsername = "xyz";
		assertEquals("", DBTest.getPassword(invalidUsername));
	}
	
	/**
	 * Test to retrieve an array containing information on a currently logged 
	 * in employee.
	 */
	@Test
	public void testCurrentEmployee(){
		String[] employeeInfo = {"1114460","Jeremy", "Peters", "Doctor", "Green", "02089978910"};
		String[] output = DBTest.currentEmployee(username);
		assertArrayEquals(employeeInfo, output);
	}
	
	/**
	 * Test to retrieve an array containing information on an invalid
	 * username.
	 */
	@Test
	public void testCurrentEmployeeInvalidUsername(){
		String invalidUsername = "gibberish";
		String[] employeeInfo = {null,null,null,null,null,null};
		String[] output = DBTest.currentEmployee(invalidUsername);
		assertArrayEquals(employeeInfo, output);
	}
	
}
