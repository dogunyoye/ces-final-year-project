package uk.ac.kcl.inf.PRJ;

/**
 * Class that stores information on each logged in user.
 * @author Dimeji
 *
 */
public class Employee {
	
	private String eID;
	private String FName;
	private String LName;

	private String Job;
	private String Category;
	private String PNumber;
	
	public Employee(String eID, String FName, String LName,  
			String Job, String Category, String PNumber){
		
		this.eID = eID;
		this.FName = FName;
		this.LName = LName;
		
		this.Job = Job;
		this.Category = Category;
		this.PNumber = PNumber;
	}
	
	/**
	 * Returns the user's EID
	 * @return user EID
	 */
	public String getEID(){
		return eID;
	}
	
	/**
	 * Returns the user's First Name
	 * @return user First name
	 */
	public String getFName(){
		return FName;
	}
	
	/**
	 * Returns the user's Last Name
	 * @return user Last name
	 */
	public String getLName(){
		return LName;
	}
	
	/**
	 * Return's the user's job
	 * @return user job
	 */
	public String getJob(){
		return Job;
	}
	
	/**
	 * Returns the user's category
	 * @return user category
	 */
	public String getCategory(){
		return Category;
	}
	
	/**
	 * Set's the user's category
	 * @param cat - name of new category to set into
	 */
	public void setCategory(String cat){
		Category = cat;
	}
	
	/**
	 * Returns the user's phone number
	 * @return user phone number
	 */
	public String getPNumber(){
		return PNumber;
	}
	
	/**
	 * Returns a concatenation of user information 
	 * @return user information
	 */
	public String toString(){
		return (eID + " - " + LName + ", " + FName +  " - " + Job + " - " + Category.toUpperCase());
	}
	
}
